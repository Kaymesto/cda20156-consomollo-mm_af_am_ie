package fr.afpa.realm;



import android.content.Context;
import android.util.Log;

import java.util.ArrayList;

import fr.afpa.beans.Aliment;
import fr.afpa.beans.Notification;
import fr.afpa.beans.Utilisateur;
import fr.afpa.session.SessionManager;
import io.realm.Realm;
import io.realm.RealmResults;

public class Helper {
    private Utilisateur utilisateur;
    private Realm realm;
    private RealmResults<Aliment> listeAliments;
    private RealmResults<Notification> listeNotifications;

    private int key;

    public Helper(Realm realm) {
        this.realm = realm;
    }

    /**
     * Récupère tous les aliments appartenant à l'utilisateur
     * @param context
     */
    public void selectFromDB(Context context) {
        //recuperer la session de l utilisateur et son id
        SessionManager sessionManager = new SessionManager(context);
        key = sessionManager.getSession();
        //attribuer la clef a l utilisateur connectee
        utilisateur = realm.where(Utilisateur.class).equalTo("idUser",key).findFirst();

        listeAliments = realm.where(Aliment.class).equalTo("utilisateur.idUser", utilisateur.getIdUser()).findAll();
    }


    /**
     * pour recuperer la liste des viandes
     * @param context
     */
    public void selectListeViandes(Context context){
       //recuperer la session de l utilisateur et son id
        SessionManager sessionManager = new SessionManager(context);
        key = sessionManager.getSession();
        //attribuer la clef a l utilisateur connectee
        utilisateur = realm.where(Utilisateur.class).equalTo("idUser",key).findFirst();
        //requete pour  recuperer les aliments type viandes
        listeAliments = realm.where(Aliment.class).equalTo("utilisateur.idUser", utilisateur.getIdUser()).equalTo("type","viandes").findAll();}


    /**
     * pour recuperer la liste des poissons
     * @param context
     */
    public void selectListePoissons(Context context){
        //recuperer la session de l utilisateur et son id
        SessionManager sessionManager = new SessionManager(context);
        key = sessionManager.getSession();
        //attribuer la clef a l utilisateur connectee
        utilisateur = realm.where(Utilisateur.class).equalTo("idUser",key).findFirst();
        listeAliments =  realm.where(Aliment.class).equalTo("utilisateur.idUser", utilisateur.getIdUser()).equalTo("type","poissons").findAll();}

    /**
     * pour recuperer la liste des laitages
     * @param context
     */
    public void selectListeLaitages(Context context){
        //recuperer la session de l utilisateur et son id
        SessionManager sessionManager = new SessionManager(context);
        key = sessionManager.getSession();
        //attribuer la clef a l utilisateur connectee
        utilisateur = realm.where(Utilisateur.class).equalTo("idUser",key).findFirst();

        listeAliments = realm.where(Aliment.class).equalTo("utilisateur.idUser", utilisateur.getIdUser()).equalTo("type","laitages").findAll();}

    /**
     * pour recuperer la liste des fruits/légumes
     * @param context
     */
    public void selectListeFruitsEtLegumes(Context context){
        //recuperer la session de l utilisateur et son id
        SessionManager sessionManager = new SessionManager(context);
        key = sessionManager.getSession();
        //attribuer la clef a l utilisateur connectee
        utilisateur = realm.where(Utilisateur.class).equalTo("idUser",key).findFirst();
        listeAliments = realm.where(Aliment.class).equalTo("utilisateur.idUser", utilisateur.getIdUser()).equalTo("type","fruits_legumes").findAll();}

    /**
     * pour recuperer la liste des divers
     * @param context
     */
    public void selectListeDivers(Context context){
        //recuperer la session de l utilisateur et son id
        SessionManager sessionManager = new SessionManager(context);
        key = sessionManager.getSession();
        //attribuer la clef a l utilisateur connectee
        utilisateur = realm.where(Utilisateur.class).equalTo("idUser",key).findFirst();
        listeAliments = realm.where(Aliment.class).equalTo("utilisateur.idUser", utilisateur.getIdUser()).equalTo("type", "divers").findAll();}


    /**
     * sélection de notifications
      * @param position
     * @return
     */
    public ArrayList<Notification> selectNotifs(int position){

           //Aliment aliment = realm.where(Aliment.class).equalTo("idAliment",position).findFirst();
            listeNotifications = realm.where(Notification.class).equalTo("aliment.idAliment",position).findAll();
           ArrayList<Notification> listeDeNotif = new ArrayList<Notification>();

          /* Notification notA = new Notification();
            notA.setIdNotif(1);
            notA.setMessage("notification 1");
            notA.setDateNotif(new java.util.Date());
            listeDeNotif.add(notA);

            Notification notB = new Notification();
            notB.setIdNotif(2);
            notB.setMessage("notification 2");
            notB.setDateNotif(new java.util.Date());
            listeDeNotif.add(notB);*/


            for (Notification notification : listeNotifications
            ) {
                listeDeNotif.add(notification);
            }

        Log.i("recherche Notif","nb Notif "+listeNotifications.size());
        Log.i("Index pos","idAliment : "+position);

            return listeDeNotif;
        }

    /**
     * Sélection de l'aliment
     * @param position
     * @return
     */
    public Aliment selectAliment(int position){
          /*  Aliment aliment = new Aliment();
            aliment.setDatePeremption("12/12/20");
            aliment.setNom("cote de porc");
            aliment.setIdAliment(position);
            aliment.setType("viandes");*/
           Aliment aliment = realm.where(Aliment.class).equalTo("idAliment",position).findFirst();
        return aliment;
        }

    /**
     * Récupération des aliments
      * @return
     */
    public ArrayList<Aliment> justRefresh() {

        ArrayList<Aliment> listeProduits = new ArrayList<>();
        for (Aliment a : listeAliments) {
            listeProduits.add(a);
        }
        //a retirer après tests ok

       // listeProduits.add(selectAliment(1));
        Log.i("recherche aliment","nb aliment "+listeProduits.size());
        return listeProduits;
    }

}
