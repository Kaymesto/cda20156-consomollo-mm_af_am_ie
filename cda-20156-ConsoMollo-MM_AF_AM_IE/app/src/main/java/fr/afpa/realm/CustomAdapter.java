package fr.afpa.realm;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import fr.afpa.beans.Aliment;
import fr.afpa.cda_20156_consomollo_mm_af_am_ie.DetailActivity;
import fr.afpa.cda_20156_consomollo_mm_af_am_ie.R;


public class CustomAdapter extends BaseAdapter {

    Context c;
    ArrayList <Aliment> listeAliments;


    public CustomAdapter(Context c, ArrayList<Aliment> listeAliments) {
        this.c = c;
        this.listeAliments = listeAliments;

    }



    @Override
    public int getCount() {
        return listeAliments.size();
    }

    @Override
    public Object getItem(int position) {
        return listeAliments.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = inflater.inflate(R.layout.list_item,parent,false);

        ImageView photo;
        TextView txtName;
        TextView txtDate;
        //ListView listeNotif;
        //il manque les nofications
        //listeNotif = view.findViewById(R.id.id_listeNotifications);
        txtName = view.findViewById(R.id.id_txMessage);
        txtDate = view.findViewById(R.id.id_tx_DateNotif);
/*
        photo = view.findViewById(R.id.photoAliment);
*/


        Aliment a =  (Aliment) this.getItem(position);

        int numPosition = a.getIdAliment();


        txtName.setText(a.getNom() );
        txtDate.setText(a.getDatePeremption());
/*
        photo.setImageDrawable(Drawable.createFromPath(a.getPhoto()));
*/

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(c, DetailActivity.class);
                intent.putExtra("numPosition", numPosition);
                c.startActivity(intent);
            }
        });

        return view;
    }
}
