package fr.afpa.realm;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import fr.afpa.beans.Aliment;
import fr.afpa.beans.Notification;
import fr.afpa.cda_20156_consomollo_mm_af_am_ie.DetailActivity;
import fr.afpa.cda_20156_consomollo_mm_af_am_ie.R;

public class CustomAdapterNotifs extends BaseAdapter{



        Context c;
        ArrayList<Notification> listeNotifs;

        public CustomAdapterNotifs(Context c, ArrayList<Notification> listeNotifs) {
            this.c = c;
            this.listeNotifs = listeNotifs;

        }



        @Override
        public int getCount() {
            return listeNotifs.size();
        }

        @Override
        public Object getItem(int position) {
            return listeNotifs.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View view, ViewGroup parent) {

            LayoutInflater inflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.liste_item_notifs,parent,false);


            TextView txtNotifs;
            TextView txtDateNotif;

            txtNotifs = view.findViewById(R.id.id_txMessage);
            txtDateNotif = view.findViewById(R.id.id_tx_DateNotif);



            Notification notification =  (Notification) this.getItem(position);

            int numPosition = notification.getIdNotif();


            txtNotifs.setText(notification.getMessage() );
            if(notification.getDateNotif() != null){
            txtDateNotif.setText(notification.getDateNotif().toString());}


        /*    view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(c, DetailActivity.class);
                    intent.putExtra("numPosition", numPosition);
                    c.startActivity(intent);
                }
            });*/

            return view;
        }
    }


