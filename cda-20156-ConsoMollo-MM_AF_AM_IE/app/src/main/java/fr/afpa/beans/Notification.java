package fr.afpa.beans;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Notification extends RealmObject implements Parcelable, Serializable {

    @PrimaryKey
    private int idNotif;
    private String message;
    private Date dateNotif;
    private Aliment aliment;


    public Notification(String message, Date dateNotif, Aliment aliment) {
        this.message = message;
        this.dateNotif = dateNotif;
        this.aliment = aliment;
    }

    public Notification() {
    }

    protected Notification(Parcel in) {
        idNotif = in.readInt();
        message = in.readString();
        aliment = in.readParcelable(Aliment.class.getClassLoader());
    }

    public static final Creator<Notification> CREATOR = new Creator<Notification>() {
        @Override
        public Notification createFromParcel(Parcel in) {
            return new Notification(in);
        }

        @Override
        public Notification[] newArray(int size) {
            return new Notification[size];
        }
    };

    public int getIdNotif() {
        return idNotif;
    }

    public void setIdNotif(int idNotif) {
        this.idNotif = idNotif;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getDateNotif() {
        return dateNotif;
    }

    public void setDateNotif(Date dateNotif) {
        this.dateNotif = dateNotif;
    }

    public Aliment getAliment() {
        return aliment;
    }

    public void setAliment(Aliment aliment) {
        this.aliment = aliment;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(idNotif);
        dest.writeString(message);
        dest.writeParcelable(aliment, flags);
    }
}
