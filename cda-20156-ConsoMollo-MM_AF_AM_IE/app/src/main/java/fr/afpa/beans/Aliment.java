package fr.afpa.beans;

import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;
import android.widget.ImageView;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Aliment extends RealmObject implements Parcelable, Serializable {

    @PrimaryKey
    private int idAliment;
    private String photo;
    private String nom;
    private String type;
    private String datePeremption;
    private Utilisateur utilisateur;
    public RealmList <Notification> listeNotifs = new RealmList<Notification>();

    public Aliment() {
        this.listeNotifs = new RealmList<Notification>();
    }

    public Aliment(String photo, String nom, String type, String datePeremption, Utilisateur utilisateur, RealmList listeNotifs) {
        this.photo = photo;
        this.nom = nom;
        this.type = type;
        this.datePeremption = datePeremption;
        this.utilisateur = utilisateur;
        this.listeNotifs = listeNotifs;
    }

    protected Aliment(Parcel in) {
        idAliment = in.readInt();
        photo = in.readString();
        nom = in.readString();
        type = in.readString();
        datePeremption = in.readString();
    }

    public static final Creator<Aliment> CREATOR = new Creator<Aliment>() {
        @Override
        public Aliment createFromParcel(Parcel in) {
            return new Aliment(in);
        }

        @Override
        public Aliment[] newArray(int size) {
            return new Aliment[size];
        }
    };

    public int getIdAliment() {
        return idAliment;
    }

    public void setIdAliment(int idAliment) {
        this.idAliment = idAliment;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDatePeremption() {
        return datePeremption;
    }

    public void setDatePeremption(String datePeremption) {
        this.datePeremption = datePeremption;
    }

    public Utilisateur getUtilisateur() {
        return utilisateur;
    }

    public void setUtilisateur(Utilisateur utilisateur) {
        this.utilisateur = utilisateur;
    }

    public RealmList<Notification> getListeNotifs() {
        return listeNotifs;
    }

    public void setListeNotifs(RealmList<Notification> listeNotifs) {
        this.listeNotifs = listeNotifs;
    }





    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(idAliment);
        dest.writeString(photo);
        dest.writeString(nom);
        dest.writeString(type);
        dest.writeString(datePeremption);
    }
}
