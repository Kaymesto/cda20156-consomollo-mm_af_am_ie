package fr.afpa.beans;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Authentification extends RealmObject {

    @PrimaryKey
    private int idAuth;
    private String login;
    private String password;


    public Authentification(String login, String password) {
        this.login = login;
        this.password = password;
    }

    public Authentification() {
    }

    public int getIdAuth() {
        return idAuth;
    }

    public void setIdAuth(int idAuth) {
        this.idAuth = idAuth;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
