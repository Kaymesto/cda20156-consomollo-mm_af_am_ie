package fr.afpa.beans;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Utilisateur extends RealmObject{

    @PrimaryKey
    private int idUser;
    private String nom;
    private String prenom;
    private Authentification authentification;

    public Utilisateur(String nom, String prenom, Authentification authentification) {
        this.nom = nom;
        this.prenom = prenom;
        this.authentification = authentification;
    }

    public Utilisateur() {
    }

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public Authentification getAuthentification() {
        return authentification;
    }

    public void setAuthentification(Authentification authentification) {
        this.authentification = authentification;
    }
}
