package fr.afpa.session;

import android.content.Context;
import android.content.SharedPreferences;

import fr.afpa.beans.Utilisateur;

public class SessionManager {

    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    String SHARED_PREF_NAME = "session";
    String SESSION_KEY = "session_user";

    public SessionManager(Context context) {
        sharedPreferences = context.getSharedPreferences(SHARED_PREF_NAME,Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
    }

    /**
     * Enregistre une session lorsque l'utilisateur se connecte
     * @param user
     */
    public void saveSession(Utilisateur user) {

        int id = user.getIdUser();

        editor.putInt(SESSION_KEY,id).commit();
    }

    /**
     * Retourne un utilisateur lorsque la session est sauvegardée
     * @return
     */
    public int getSession() {
       return sharedPreferences.getInt(SESSION_KEY, -1);

    }

    public void removeSession() {
        editor.putInt(SESSION_KEY,-1).commit();
    }
}
