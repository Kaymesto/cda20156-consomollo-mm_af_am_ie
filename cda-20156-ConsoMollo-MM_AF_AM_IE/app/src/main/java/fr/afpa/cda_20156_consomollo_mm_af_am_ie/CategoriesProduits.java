package fr.afpa.cda_20156_consomollo_mm_af_am_ie;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Switch;

import java.util.ArrayList;
import java.util.logging.Logger;

import fr.afpa.beans.Aliment;
import fr.afpa.model.GestionAliment;
import io.realm.Realm;
import io.realm.RealmResults;

/*
class permettant d afficher les differentes categories de produits à référencer pour l app ConsoMollo
 */
public class CategoriesProduits extends AppCompatActivity implements View.OnClickListener {

   //creation des attributs en fonction des types d objets possibles ds l activity
    private ImageButton image_Viande;
    private ImageButton image_Poissons;
    private ImageButton image_Laitage;
    private ImageButton image_FruitsLegumes;
    private ImageButton image_Divers;
    private String typage;

    private ArrayList listeAliments;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_categories_produits);

        //Association des attributs de la classe avec les id correspondant aux imagesButton de l activity
     image_Viande = findViewById(R.id.id_Image_Viande);
     image_Poissons = findViewById(R.id.id_Image_Poissons);
     image_Laitage = findViewById(R.id.id_Image_Laitage);
     image_FruitsLegumes = findViewById(R.id.id_Image_FruitsLegumes);
     image_Divers = findViewById(R.id.id_Divers);
        image_Viande.setOnClickListener(this);
        image_Poissons.setOnClickListener(this);
        image_Laitage.setOnClickListener(this);
        image_FruitsLegumes.setOnClickListener(this);
        image_Divers.setOnClickListener(this);

    }
/*
en fonction du clic sur l image, des donnees seront envoyees pour connaitre quel type de
liste a afficher ds l activity listeproduit
 */

    @Override
    public void onClick(View v) {
        Intent listeProduits = new Intent(this,ListeProduits.class);
        Log.i("recherche v.get(id)","ressource"+v.getId());

       switch (v.getId()) {
           case R.id.id_Image_Viande :
               typage = "viandes";
               listeProduits.putExtra("listeProduit",typage);
               startActivity(listeProduits);
               break;
           case R.id.id_Image_Poissons :
               typage = "poissons";
               listeProduits.putExtra("listeProduit",typage);
               startActivity(listeProduits);
               break;
           case R.id.id_Image_Laitage:
               typage = "laitages";
               listeProduits.putExtra("listeProduit",typage);
               startActivity(listeProduits);
               break;
           case R.id.id_Image_FruitsLegumes:
               typage = "fruits_legumes";
               listeProduits.putExtra("listeProduit",typage);
               startActivity(listeProduits);
               break;
           case R.id.id_Divers:
               typage = "divers";
               listeProduits.putExtra("listeProduit",typage);
               startActivity(listeProduits);
               break;
           default:
               throw new IllegalStateException("Unexpected value: " + v);
       }
    }
}