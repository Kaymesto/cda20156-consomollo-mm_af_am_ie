package fr.afpa.cda_20156_consomollo_mm_af_am_ie;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.EditText;
import android.widget.TextView;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import fr.afpa.beans.Aliment;
import fr.afpa.beans.Notification;
import fr.afpa.beans.Utilisateur;
import fr.afpa.model.GestionAliment;
import fr.afpa.model.GestionNotification;
import fr.afpa.session.SessionManager;
import io.realm.Realm;
import io.realm.RealmList;

public class ajouter_notifications extends AppCompatActivity {
    private TextView affichageDate;
    private EditText messageNotification;
    private CalendarView choixDeDate;
    private Button boutonValider;
    private Button boutonAjoutNotification;
    private Button boutonRetour;
    private GestionNotification gestionNotification;
    private Intent intent;
    private Intent intentRecupAliment;
    private Realm realm;
    private Aliment aliment;
    private int compteurNumeroNotif=0;
    private boolean isNumeroNotifRecupere = false;
    private ArrayList<Notification> listeNotifsAL;
    private ArrayList<String> listeDateNotif;
    private GestionAliment gestionAliment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ajouter_notifications);
        intentRecupAliment = getIntent();
        listeDateNotif = (ArrayList<String>) intentRecupAliment.getSerializableExtra("listeDateNotif");
        if(listeDateNotif==null){
        listeDateNotif = new ArrayList<String>();}
        aliment= (Aliment) intentRecupAliment.getSerializableExtra("aliment");
        Bundle args = intentRecupAliment.getBundleExtra("BUNDLE");
        if(args!=null){
            listeNotifsAL = (ArrayList<Notification>) args.getSerializable("ARRAYLIST");
            Log.i("VERIF BUNDLE","OKKKKKKKKKKKK");}

       // listeNotifsAL= (ArrayList<Notification>) intentRecupAliment.getSerializableExtra("listeNotif");
        if(listeNotifsAL == null){
            listeNotifsAL = new ArrayList<Notification>();
        }

        for(Notification n : listeNotifsAL){
            Log.i("DATE Notif", ""+n.getDateNotif());

        }
        Log.i("TAILLE LISTE", ""+listeNotifsAL.size());
        choixDeDate = findViewById(R.id.calendrier);
        messageNotification = findViewById(R.id.messageNotif);
        boutonValider = findViewById(R.id.boutonValider);
        boutonAjoutNotification = findViewById(R.id.boutonAjoutNotif);
        boutonRetour = findViewById(R.id.boutonRetour);
        affichageDate = findViewById(R.id.affichageDate);

        choixDeDate.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(CalendarView CalendarView, int jour, int mois, int annee) {
                String date = annee + "/" + (mois +1) + "/"+ jour ;
                affichageDate.setText(date);


            }
        });


        boutonValider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(v.getContext(),ConfirmationCreationAliment.class);


                fr.afpa.beans.Notification notification = new Notification();
                notification.setMessage(messageNotification.getText().toString());
                Date date1 = null;
                try {
                    date1=new SimpleDateFormat("dd/MM/yyyy").parse(affichageDate.getText().toString());
                    Log.i("VERIFICATION CREATION DATE", ""+date1);

                } catch (ParseException e) {
                    e.printStackTrace();
                }
                notification.setDateNotif(date1);
                notification.setAliment(aliment);
                aliment.getListeNotifs().add(notification);
                gestionAliment = new GestionAliment();
                boolean verification = gestionAliment.createAliment(aliment,v.getContext(),listeNotifsAL,notification, listeDateNotif);
                Log.i("VERIFICATION CREATION ALIMENT", ""+verification);

/*
                realm = Realm.getDefaultInstance();
                Log.i("VERIFICATION REALM ", ""+realm.isInTransaction());
                Log.i("VERIFICATION REALM ", ""+realm.isClosed());


                try {

                    Number maxIdNotif = realm.where(Notification.class).max("idNotif");

                    int newKeyNotif = (maxIdNotif == null) ? 1 : maxIdNotif.intValue() + 1;
                    for (Notification n: listeNotifsAL) {
                        if(n.getIdNotif()<newKeyNotif){
                        n.setIdNotif(newKeyNotif);
                        newKeyNotif++;}
                        notification.setAliment(aliment);
                    }
                    notification.setDateNotif(date1);
                            // erreur, ne pas passer par le get
                            // aliment.getListeNotifs().add(notification);
                            //aliment.listeNotifs.add(notification);

                    //affectation des notifications
                    for (Notification notif : listeDeNotifications){
                        aliment.getListeNotifs().add(notif);
                        notif.setAliment(aliment);

                        Log.i("notif", "notif: "+notif.getIdNotif());
                    }
                    Log.i("verif liste de notif", "onClick: "+listeDeNotifications.size());
                    realm.copyToRealm(aliment);

                    realm.commitTransaction();
                    realm.close();


                }
                catch (Exception e){
                    realm.close();

                }*/
                finish();
                startActivity(intent);
            }

        });

        boutonAjoutNotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ("Date choisie".equals(affichageDate.getText().toString()) == false) {
                    intent = new Intent(v.getContext(), ajouter_notifications.class);
                    Notification notification = new Notification();
                    notification.setMessage(messageNotification.getText().toString());
                    listeDateNotif.add(affichageDate.getText().toString());
                    Date date1 = null;
                    try {
                        date1 = new SimpleDateFormat("dd/MM/yyyy").parse(affichageDate.getText().toString());
                        Log.i("VERIFICATION CREATION DATE1", "" + date1);
                        notification.setDateNotif(date1);
                        Log.i("VERIFICATION CREATION DATE2", "" + notification.getDateNotif());

                    } catch (ParseException e) {
                        e.printStackTrace();
                        Log.i("VERIFICATION CREATION DATE3", "DATE PAS CREEE");
                    }
                    Realm.init(v.getContext());


                    realm = Realm.getDefaultInstance();
                    try {
                        if (realm.isInTransaction() == false) {
                            realm.beginTransaction();
                        }
/*
                    if(!isNumeroNotifRecupere) {
*/
                        Number maxIdNotif = realm.where(Notification.class).max("idNotif");
                        compteurNumeroNotif = (maxIdNotif == null) ? 1 : maxIdNotif.intValue() + listeNotifsAL.size() + 1;
                        isNumeroNotifRecupere = true;
                        notification.setIdNotif(compteurNumeroNotif);
                        listeNotifsAL.add(notification);
                        // Erreur d'association ne pas passer par le get
                        // aliment.getListeNotifs().add(notification);
                        // aliment.listeNotifs.add(notification);
                        // notification.setAliment(aliment);
                        realm.commitTransaction();
  /*                  }else{
                        compteurNumeroNotif++;
                    }*/

                    } finally {

                        realm.close();

                    }
                    aliment.getListeNotifs().add(notification);
                    intent.putExtra("aliment", (Serializable) aliment);
                    ArrayList<Object> object = new ArrayList<Object>();
                    Bundle args = new Bundle();
                    args.putSerializable("ARRAYLIST", (Serializable) listeNotifsAL);
                    intent.putExtra("BUNDLE", args);
                    intent.putExtra("listeNotif", listeNotifsAL);
                    intent.putExtra("listeDateNotif", listeDateNotif);
                    finish();
                    startActivity(intent);
                }
            }
        });



        boutonRetour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(v.getContext(), EspaceClient.class);
                finish();
                startActivity(intent);
            }});


    }
}