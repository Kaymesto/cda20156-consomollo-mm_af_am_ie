package fr.afpa.cda_20156_consomollo_mm_af_am_ie;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import fr.afpa.beans.Authentification;
import fr.afpa.beans.Utilisateur;
import fr.afpa.model.GestionUtilisateur;

public class FormulaireCreationCompte extends AppCompatActivity {

    EditText nom;
    EditText prenom;
    EditText login;
    EditText password;
    Button creation;
    Button retour;
    TextView messageErreur;
    Intent intent;
    Utilisateur utilisateur;
    GestionUtilisateur gestionUtilisateur;
    Authentification auth;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_formulaire_creation_compte);

        nom = findViewById(R.id.txt_nom_user);
        prenom = findViewById(R.id.txt_surname_user);
        login = findViewById(R.id.txt_login_user);
        password = findViewById(R.id.txt_pwd_user);
        creation = findViewById(R.id.creationBtn);
        retour = findViewById(R.id.returnAccueilConnexion);
        messageErreur = findViewById(R.id.erreurCreation);

        intent = new Intent(this,ConfirmationCreationCompte.class);

        creation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                gestionUtilisateur = new GestionUtilisateur();
                utilisateur = new Utilisateur();
                auth = new Authentification();
                String name = nom.getText().toString();
                String surname = prenom.getText().toString();
                String log = login.getText().toString();
                String mDP = password.getText().toString();
                if (name.isEmpty()==false && surname.isEmpty()== false && log.isEmpty()== false && mDP.isEmpty()== false ) {
                auth.setLogin(log);
                auth.setPassword(mDP);
                utilisateur.setNom(name);
                utilisateur.setPrenom(surname);
                utilisateur.setAuthentification(auth);

                if(gestionUtilisateur.checkLoginExist(utilisateur, v.getContext())){
                    messageErreur.setText("Ce login est déja utilisé");
                }

                else  if (gestionUtilisateur.createAccount(utilisateur, v.getContext()) == true){
                        startActivity(intent);}
                }

                else {
                    messageErreur.setText("Merci de remplir l'intégralité des champs");
                };

            }
        });

        retour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intentAccueil = new Intent(v.getContext(), MainActivity.class);
                startActivity(intent);

            }
        });
    }

}
