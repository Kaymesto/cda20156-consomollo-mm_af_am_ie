package fr.afpa.cda_20156_consomollo_mm_af_am_ie;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;

import io.realm.Realm;

public class FicheProduit extends AppCompatActivity {
    private ImageView imageProduit;
    private EditText nomProduit;
    private EditText datePeremption;
    private ListView listeNotifications;
    private Button ajoutModif;
    private Button modif;
    private Realm realm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fiche_produit);
        imageProduit = findViewById(R.id.id_ImageProduit);
        nomProduit=findViewById(R.id.id_nomDuProduit);
        datePeremption=findViewById(R.id.id_DatePeremption);
        listeNotifications=findViewById(R.id.id_listeNotifications);
        ajoutModif=findViewById(R.id.id_BtnAjoutNotif);
        modif=findViewById(R.id.id_BtnModifications);


    }
}