package fr.afpa.cda_20156_consomollo_mm_af_am_ie;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class ConfirmationCreationAliment extends AppCompatActivity {

    private Button btn_retour_espaceClient;
    private Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirmation_creation_aliment);

        btn_retour_espaceClient = findViewById(R.id.btn_retour_espace_client_accueil);


        intent = new Intent(this,EspaceClient.class);


        btn_retour_espaceClient.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(intent);


            }
        });

    }
}