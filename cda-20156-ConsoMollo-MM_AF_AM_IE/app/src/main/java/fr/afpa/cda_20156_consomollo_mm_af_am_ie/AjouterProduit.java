
package fr.afpa.cda_20156_consomollo_mm_af_am_ie;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.Serializable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import fr.afpa.beans.Aliment;
import fr.afpa.beans.Notification;
import fr.afpa.beans.Utilisateur;
import fr.afpa.session.SessionManager;
import io.realm.Realm;
import io.realm.RealmList;

public class AjouterProduit extends AppCompatActivity {
    private ImageView imageDuProduit;
    private EditText nomProduit;
    private TextView dateDePeremption;
    private Button boutonValider;
    private Button boutonRetour;
    private TextView messageErreur;
    private Aliment aliment;
    private CheckBox btnViandes;
    private CheckBox btnPoissons;
    private CheckBox btnLaitages;
    private CheckBox btnFruitsLegumes;
    private Intent intent;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ajouter_produit);

        imageDuProduit = findViewById(R.id.image_produit);
        nomProduit = findViewById(R.id.nomDuProduit);
        dateDePeremption = findViewById(R.id.dateDePeremption);
        boutonValider  = findViewById(R.id.boutonAjoutNotif);
        boutonRetour = findViewById(R.id.boutonRetour);
        messageErreur = findViewById(R.id.msg_erreur_addproduct);


        btnFruitsLegumes = findViewById(R.id.checkBoxFruitsLegumes);
        btnLaitages = findViewById(R.id.checkBoxLait);
        btnPoissons = findViewById(R.id.checkBoxPoissons);
        btnViandes = findViewById(R.id.checkBoxViandes);

        btnViandes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!btnViandes.isChecked()) {

                    btnFruitsLegumes.setChecked(false);
                    btnLaitages.setChecked(false);
                    btnPoissons.setChecked(false);
                }

                else {
                    btnFruitsLegumes.setChecked(false);
                    btnLaitages.setChecked(false);
                    btnPoissons.setChecked(false);
                }


            }
        });


        btnPoissons.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (btnPoissons.isChecked()) {

                    btnFruitsLegumes.setChecked(false);
                    btnLaitages.setChecked(false);
                    btnViandes.setChecked(false);
                }

                    btnLaitages.setChecked(false);


            }
        });

        btnLaitages.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (btnLaitages.isChecked()) {

                    btnFruitsLegumes.setChecked(false);
                    btnPoissons.setChecked(false);
                    btnViandes.setChecked(false);
                }

                else {
                    btnFruitsLegumes.setChecked(false);
                    btnPoissons.setChecked(false);
                    btnViandes.setChecked(false);
                }


            }
        });

        btnFruitsLegumes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (btnFruitsLegumes.isChecked()) {

                    btnLaitages.setChecked(false);
                    btnPoissons.setChecked(false);
                    btnViandes.setChecked(false);
                }

                else {
                    btnLaitages.setChecked(false);
                    btnPoissons.setChecked(false);
                    btnViandes.setChecked(false);
                }


            }
        });




        boutonValider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                aliment = new Aliment();
                String nom = nomProduit.getText().toString();
                String datePeremption = dateDePeremption.getText().toString();
                Pattern pattern = Pattern.compile("(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\\d\\d");
                Matcher matcher = pattern.matcher(datePeremption);

                if(matcher.find()){
                if(nom.isEmpty() != true && datePeremption.isEmpty() != true){

                aliment.setDatePeremption(datePeremption);
                aliment.setNom(nom);

                    if (btnLaitages.isChecked()) {
                        aliment.setType("laitages");
                    }

                    else if (btnViandes.isChecked()) {
                        aliment.setType("viandes");
                    }

                    else if (btnFruitsLegumes.isChecked()) {
                        aliment.setType("fruits_legumes");
                    }

                    else if (btnPoissons.isChecked()) {
                        aliment.setType("poissons");
                    }

                    else {
                        aliment.setType("divers");
                    }
                    Realm realm = null;
                    realm = Realm.getDefaultInstance();

                    try {

                        Number maxId = realm.where(Aliment.class).max("idAliment");
                        int newKey = (maxId == null) ? 1 : maxId.intValue() + 1;
                        aliment.setIdAliment(newKey);
                        SessionManager sessionManager = new SessionManager(v.getContext());
                        int key = sessionManager.getSession();
                        Utilisateur utilisateur = realm.where(Utilisateur.class).equalTo("idUser",key).findFirst();
                        aliment.setUtilisateur(utilisateur);

                        realm.close();


                    }
                    catch (Exception e){
                        realm.close();

                    }
                intent = new Intent(v.getContext(), ajouter_notifications.class);
                    aliment.setListeNotifs(new RealmList<Notification>());
                    intent.putExtra("aliment", (Serializable) aliment);
                startActivity(intent);}
                else {
                    messageErreur.setText("Merci de remplir l'intégralité des champs.");
                }}
                else {
                    messageErreur.setText("Merci de respecter le format de date suivant : dd/MM/yyyy");
                }
            }});


        boutonRetour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(v.getContext(), EspaceClient.class);
                finish();
                startActivity(intent);
            }});


    }
}
/*
        realm = Realm.getDefaultInstance();


        ajouterUnePhoto = findViewById(R.id.ajouterUnePhoto);
        nomDuProduit = findViewById(R.id.nomDuProduit);
        dateDePeremption = findViewById(R.id.dateDePeremption);
        boutonValider = findViewById(R.id.boutonValider);
        messageErreur = findViewById(R.id.erreur);
        intent = new Intent(this,ConfirmationCreationCompte.class);


        //Permission pour la prise de photos
       // if (ContextCompat.checkSelfPermission(AjouterProduit.this,
       //         !Manifest.permission.CAMERA.equals(PackageManager.PERMISSION_GRANTED))){

           /* ActivityCompat.requestPermissions(AjouterProduit.this,
            new String[]{
                    Manifest.permission.CAMERA
            },
                    validateRequestPermissionsRequestCode(100);

        }


        ajouterUnePhoto.setOnClickListener(new View.OnClickListener() {
            //ouvrir la caméra
            //@Override
           // public void onClick(View v) {
          //      intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
          //      startActivityForResult(intent,validateRequestPermissionsRequestCode(100));
          //  }
       // });




        ajouterUneNotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


            }
        });


*/














