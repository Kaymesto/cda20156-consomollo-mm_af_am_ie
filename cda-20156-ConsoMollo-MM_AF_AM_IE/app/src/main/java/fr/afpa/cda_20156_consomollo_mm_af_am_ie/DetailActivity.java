package fr.afpa.cda_20156_consomollo_mm_af_am_ie;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;

import fr.afpa.beans.Aliment;
import fr.afpa.beans.Notification;
import fr.afpa.realm.CustomAdapterNotifs;
import fr.afpa.realm.Helper;
import io.realm.Realm;

public class DetailActivity extends AppCompatActivity {

    Realm realm;
    EditText editNom;
    EditText editDate;
    ImageView editPhoto;
    ListView listeNotif;
    ArrayList<Notification> listeDeNotif;
    Button update;
    Button delete;
    Aliment aliment;

    Intent view;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        realm = Realm.getDefaultInstance();
        listeNotif = findViewById(R.id.id_listeNotifications);
        editNom = findViewById(R.id.edit_nom);
        editDate = findViewById(R.id.edit_date);
/*
        editPhoto = findViewById(R.id.edit_photo);
*/
        update = findViewById(R.id.update);
        delete = findViewById(R.id.delete);


        Intent intent = getIntent();
        int position = intent.getIntExtra("numPosition",0);
        //creer une instance de realm
        Realm.init(this);
        realm = Realm.getDefaultInstance();



        Helper helper = new Helper(realm);

        listeDeNotif = helper.selectNotifs(position);
        CustomAdapterNotifs adapterNotifs = new CustomAdapterNotifs(this,listeDeNotif);
        aliment = helper.selectAliment(position);
        listeNotif.setAdapter(adapterNotifs);

        editNom.setText(aliment.getNom());
        editDate.setText((CharSequence) aliment.getDatePeremption());
/*
        editPhoto.setImageDrawable(Drawable.createFromPath(aliment.getPhoto()));
*/

        view = new Intent(this,ListeProduits.class);
        //manque l'ajout du type pour la liste desproduits à afficher
        view.putExtra("listeProduit",aliment.getType());


        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateData();
                startActivity(view);
                finish();
            }
        });

        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteData();
                startActivity(view);
                finish();
            }
        });


    }

    private void updateData() {

        if(realm.isInTransaction()==false){
        realm.beginTransaction();}

        aliment.setNom(editNom.getText().toString());
        aliment.setDatePeremption(editDate.getText().toString());
        aliment.setPhoto(String.valueOf(Drawable.createFromPath(aliment.getPhoto())));
        aliment.setListeNotifs(aliment.getListeNotifs());

        realm.commitTransaction();
    }

    private void deleteData() {
        realm.beginTransaction();
        aliment.deleteFromRealm();
        realm.commitTransaction();
    }


}