package fr.afpa.cda_20156_consomollo_mm_af_am_ie;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import fr.afpa.beans.Authentification;
import fr.afpa.beans.Utilisateur;
import fr.afpa.model.GestionUtilisateur;

public class ConfirmationCreationCompte extends AppCompatActivity {

    Button btn_retour;
    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirmation_creation_compte);

        btn_retour = findViewById(R.id.returnAccueilConnexion);


        intent = new Intent(this,MainActivity.class);

        btn_retour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(intent);

            }
        });

    }
}