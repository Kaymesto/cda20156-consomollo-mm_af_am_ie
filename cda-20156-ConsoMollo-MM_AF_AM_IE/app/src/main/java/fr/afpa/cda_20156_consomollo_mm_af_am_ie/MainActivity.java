package fr.afpa.cda_20156_consomollo_mm_af_am_ie;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import fr.afpa.beans.Authentification;
import fr.afpa.cda_20156_consomollo_mm_af_am_ie.NotificationService;
import fr.afpa.beans.Utilisateur;
import fr.afpa.model.GestionAuthentification;
import fr.afpa.session.SessionManager;
import io.realm.Realm;

public class MainActivity extends AppCompatActivity {

    EditText txtLogin;
    EditText txtPassword;
    Button connexion;
    TextView inscription;
    TextView error;
    Intent intent;
    Intent intentFormulaireCreationCompte;
    GestionAuthentification gestionAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtLogin = findViewById(R.id.txt_nom_user);
        txtPassword = findViewById(R.id.txt_pwd_user);
        connexion = findViewById(R.id.connexionBtn);
        inscription = findViewById(R.id.id_txt_liencreationcompte);
        error = findViewById(R.id.id_errorConnexion);

        intent = new Intent(this,EspaceClient.class);
        intentFormulaireCreationCompte = new Intent(this,FormulaireCreationCompte.class);
        startService(new Intent(this, NotificationService.class));

        connexion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                gestionAuth = new GestionAuthentification();
                
                Utilisateur utilisateur = gestionAuth.sendUser(txtLogin.getText().toString(), txtPassword.getText().toString(), v.getContext());
                if (utilisateur != null) {
                    SessionManager sessionManager = new SessionManager(MainActivity.this);
                    sessionManager.saveSession(utilisateur);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                    finish();
                    startActivity(intent);

                }

                else {
                    error.setText("Identifiant/Mot de passe incorrect");
                };

            }
        });

        inscription.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(intentFormulaireCreationCompte);


            }
        });


    }

    @Override
    protected void onStart() {
        super.onStart();
        //checkSession();
    }


    /**
     * Verifie si la session est ouverte au demarrage de l'application
     */
    private void checkSession() {

    SessionManager sessionManager = new SessionManager(MainActivity.this);
    int userId = sessionManager.getSession();
    // Si l'id ne vaut pas la valeur par défaut, on le redirige vers l'espace client
        if (userId != -1) {
        startActivity(intent);
    }

        else {
            // Sinon on ne fait rien (on attend qu'il se connecte)
        }
    }
}