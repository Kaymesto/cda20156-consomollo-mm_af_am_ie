package fr.afpa.cda_20156_consomollo_mm_af_am_ie;

import android.annotation.SuppressLint;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import java.lang.reflect.Array;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import fr.afpa.beans.Aliment;
import fr.afpa.beans.Notification;
import fr.afpa.model.GestionAliment;
import fr.afpa.model.GestionNotification;

public class NotificationService extends Service {

    Timer timer;
    TimerTask timerTask;
    String TAG = "Timers";
    int Your_X_SECS = 86400;
    GestionAliment gestionAliment;


    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }

    /**
     * Lors du démarrage
     * @param intent
     * @param flags
     * @param startId
     * @return
     */
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.e(TAG, "onStartCommand");
        super.onStartCommand(intent, flags, startId);

        startTimer();

        return START_STICKY;
    }


    /**
     * Lors de la création on envoie un message
     */
    @Override
    public void onCreate() {
        Log.e(TAG, "onCreate");


    }

    /**
     * Lors de la destruction on envoie un message
     */
    @Override
    public void onDestroy() {
        Log.e(TAG, "onDestroy");
        stoptimertask();
        super.onDestroy();


    }

    //we are going to use a handler to be able to run in our TimerTask
    final Handler handler = new Handler();


    /**
     * Démarrage du timer
     */
    public void startTimer() {
        //set a new Timer
        timer = new Timer();

        //initialize the TimerTask's job
        initializeTimerTask();

        //schedule the timer, after the first 5000ms the TimerTask will run every 10000ms
        timer.schedule(timerTask, 5000, Your_X_SECS * 1000); //
        //timer.schedule(timerTask, 5000,1000); //
    }

    /**
     * Arrêt du timer
     */
    public void stoptimertask() {
        //stop the timer, if it's not already null
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
    }

    /**
     * Initialisation de la génération des notifications
     */
    public void initializeTimerTask() {

        timerTask = new TimerTask() {
            public void run() {

                //use a handler to run a toast that shows the current timestamp
                handler.post(new Runnable() {
                    public void run() {
                        Log.i("JE SUIS DANS LE RUN DE NOTIF", "OKKKKKKKKKKKKKKK");
                        //TODO CALL NOTIFICATION FUNC
                        Date now = new Date();
                        gestionAliment = new GestionAliment();
                        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/YYYY");
                        String jourActuel = formatter.format(now);
                        Log.i("Date SYSTEME", "======>"+jourActuel);
                        int compteur = 0;
                        ArrayList<Aliment> listeAliment = new ArrayList<Aliment>();
                        listeAliment = gestionAliment.recupAllAliments(getApplicationContext());
                        Log.i("TAILLE LISTE ALIMENTS", "======>"+listeAliment.size());
                        for (Aliment aliment: listeAliment) {
                            Log.i("TAILLE NOM ALIMENT", "======>"+aliment.getNom());
                            Log.i("TAILLE LISTE NOTIF PAR ALIMENT", "======>"+aliment.getListeNotifs().size());

                            GestionNotification gestionNotification = new GestionNotification();

                            ArrayList<Notification> listeNotifs = gestionNotification.recupAllNotification(getApplicationContext(), aliment);
                            for(Notification notification : listeNotifs){
                                Log.i("DATE NOTIFICATION", "DATE======>"+notification.getDateNotif());
                                if(notification.getDateNotif()!= null){
                                    String jourNotif = formatter.format(notification.getDateNotif());
                                    Log.i("DATE NOTIFICATION", "======>"+jourNotif);


                                    if(jourActuel.equals(jourNotif)){
                                        Log.i("ENTRE DANS LA CONDITION", "BOOLEAN ======>"+jourActuel.equals(jourNotif));
                                        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(getApplicationContext())
                                                .setSmallIcon(R.drawable.banniere) // notification icon
                                                .setContentTitle(""+aliment.getNom()) // title
                                                .setContentText(""+notification.getMessage()) // body message
                                                .setAutoCancel(true);

                                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                        @SuppressLint("WrongConstant") PendingIntent pi = PendingIntent.getActivity(getApplicationContext(), 0, intent, Intent.FLAG_ACTIVITY_NEW_TASK);
                                        mBuilder.setContentIntent(pi);
                                        NotificationManager mNotificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
                                        mNotificationManager.notify(compteur, mBuilder.build());
                                        compteur++;
                                        Log.i("PASSAGE APRES CREATION NOTIFICATION","=======> OKAY");
                                    }
                                }

                            }

                        }

                    }
                });
            }
        };
    }
}