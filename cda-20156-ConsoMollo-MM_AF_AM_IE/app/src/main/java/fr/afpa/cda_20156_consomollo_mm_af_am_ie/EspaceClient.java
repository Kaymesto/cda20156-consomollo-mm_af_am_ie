package fr.afpa.cda_20156_consomollo_mm_af_am_ie;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import fr.afpa.realm.CustomAdapter;
import fr.afpa.realm.Helper;
import io.realm.Realm;
import io.realm.RealmChangeListener;

public class EspaceClient extends AppCompatActivity {
    private Button rechercher;
    private ImageView add;
    private TextView msgListeVide;
    private ListView listeProduits;
    private Intent ajoutProduit;
    private Intent categoriesProduit;
    Realm realm;
    Helper helper;
    RealmChangeListener realmChangeListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_espace_client);

        rechercher = findViewById(R.id.creationBtn);
        add = findViewById(R.id.id_imgAdd);
        msgListeVide = findViewById(R.id.msg_alert_empty);
        listeProduits = findViewById(R.id.id_listAliments);
        ajoutProduit = new Intent(this, AjouterProduit.class);
        categoriesProduit = new Intent(this, CategoriesProduits.class);

        rechercher.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(categoriesProduit);
            }
        });


        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(ajoutProduit);
            }
        });

        Realm.init(this);
        realm = Realm.getDefaultInstance();
        helper = new Helper(realm);

        helper.selectFromDB(this);

        CustomAdapter adapter = new CustomAdapter(this, helper.justRefresh());
        listeProduits.setAdapter(adapter);
        refresh();

        if (adapter.getCount() != 0) {
            msgListeVide.setText("");
        }

    }

    private void refresh() {
        realmChangeListener = new RealmChangeListener() {
            @Override
            public void onChange(Object o) {
                CustomAdapter adapter = new CustomAdapter(EspaceClient.this,helper.justRefresh());
                listeProduits.setAdapter(adapter);
            }
        };
        realm.addChangeListener(realmChangeListener);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        realm.removeChangeListener(realmChangeListener);
        realm.close();
    }
}