package fr.afpa.cda_20156_consomollo_mm_af_am_ie;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import fr.afpa.realm.CustomAdapter;
import fr.afpa.realm.Helper;
import io.realm.Realm;
import io.realm.RealmChangeListener;

public class ListeProduits extends AppCompatActivity {
    ListView listProduits;
    TextView message;
    Realm realm;
    Helper helper;
    RealmChangeListener realmChangeListener;
    ImageView ajoutProduit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_liste_produits);
        //creer une instance de realm
        Realm.init(this);
        realm = Realm.getDefaultInstance();
        //Association des attributs de la classe avec les id correspondant aux imagesButton de l activity
        listProduits = findViewById(R.id.id_listAliments);
        message = findViewById(R.id.msg_alert_empty);
        ajoutProduit = findViewById(R.id.id_imgAdd);
        //recuperation des elements de la page precedente
        Intent intent = getIntent();
        helper = new Helper(realm);
        Intent ajouterProduit = new Intent(this,AjouterProduit.class);

    ajoutProduit.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {

        startActivity(ajouterProduit);
    }
});

        switch (intent.getStringExtra("listeProduit")) {

            case "viandes":

                helper.selectListeViandes(ListeProduits.this);
                CustomAdapter adapterViandes = new CustomAdapter(this, helper.justRefresh());
                listProduits.setAdapter(adapterViandes);
                refreshProduits();
                if (adapterViandes.getCount() != 0) {
                    message.setText("");
                }

                break;

            case "poissons":
                helper.selectListePoissons(ListeProduits.this);
                CustomAdapter adapterPoissons = new CustomAdapter(this, helper.justRefresh());
                listProduits.setAdapter(adapterPoissons);
                refreshProduits();
                if (adapterPoissons.getCount() != 0) {
                    message.setText("");
                }
                break;

            case "laitages":
                helper.selectListeLaitages(ListeProduits.this);
                CustomAdapter adapterLaitages = new CustomAdapter(this, helper.justRefresh());
                listProduits.setAdapter(adapterLaitages);
                refreshProduits();
                if (adapterLaitages.getCount() != 0) {
                    message.setText("");
                }
                break;

            case "fruits_legumes":
                helper.selectListeFruitsEtLegumes(ListeProduits.this);
                CustomAdapter adapterFruitsLegumes = new CustomAdapter(this, helper.justRefresh());
                listProduits.setAdapter(adapterFruitsLegumes);
                refreshProduits();
                if (adapterFruitsLegumes.getCount() != 0) {
                    message.setText("");
                }
                break;
            case "divers":
                helper.selectListeDivers(ListeProduits.this);
                CustomAdapter adapterDivers = new CustomAdapter(this, helper.justRefresh());
                listProduits.setAdapter(adapterDivers);
                refreshProduits();
                if (adapterDivers.getCount() != 0) {
                    message.setText("");
                }
                break;
            default:
                break;
        }
    }
        private void refreshProduits() {
            realmChangeListener = new RealmChangeListener() {
                @Override
                public void onChange(Object o) {
                    CustomAdapter adapter = new CustomAdapter(ListeProduits.this,helper.justRefresh());
                    listProduits.setAdapter(adapter);
                }
            };
            realm.addChangeListener(realmChangeListener);
        }


    }

