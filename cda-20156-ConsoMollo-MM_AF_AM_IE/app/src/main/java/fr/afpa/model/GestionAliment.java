package fr.afpa.model;

import android.content.Context;
import android.util.Log;

import fr.afpa.beans.Aliment;
import fr.afpa.beans.Authentification;
import fr.afpa.beans.Notification;
import fr.afpa.beans.Utilisateur;
import fr.afpa.cda_20156_consomollo_mm_af_am_ie.AjouterProduit;
import fr.afpa.cda_20156_consomollo_mm_af_am_ie.MainActivity;
import fr.afpa.session.SessionManager;
import io.realm.Realm;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import io.realm.RealmList;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import io.realm.internal.Util;

/*
class de la gestion des aliments avec leurs differents services
 */
public class GestionAliment {
    private Realm realm;
    private ArrayList<Aliment> listeProduits;
    private SessionManager sessionManager;

    /**
     *     Méthode pour créer un aliment dans la mémoire(pas sûr)
     */
    public boolean creerAliment(Aliment aliment, Context context) {
        // Initialize Realm (just once per application)
        Realm.init(context);

// Get a Realm instance for this thread
        realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        try {
            realm.copyToRealm(aliment);
            realm.commitTransaction();
            realm.close();
            return true;
        }
        catch (Exception e){
            realm.close();
            return false;
        }
    }
/////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * méthode pour créer un aliment(pas sûr)
     * @param photo
     * @param nom
     * @param type
     * @param dateDePeremption
     * @param utilisateur
     * @param listeNotifs
     */
    private void SaveData(String photo, String nom, String type, String dateDePeremption, Utilisateur utilisateur, RealmList<Notification> listeNotifs) {
        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm bgRealm) {
                Number maxId = bgRealm.where(Aliment.class).max("idAliment");
                int newKey = (maxId == null) ? 1 : maxId.intValue() + 1;

                Aliment aliment = bgRealm.createObject(Aliment.class);
                aliment.setPhoto(photo);
                aliment.setNom(nom);
                aliment.setType(type);
                aliment.setDatePeremption(dateDePeremption);
                aliment.setUtilisateur(utilisateur);//ici est-ce que c'est bon?
                //pas fini reste la liste de notifications

            }
        }, new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
                // Transaction was a success.
            }
        }, new Realm.Transaction.OnError() {
            @Override
            public void onError(Throwable error) {
                // Transaction failed and was automatically canceled.
            }
        });

    }


    //////////////////////////////////ADRIEN///////////////////////////////////////////////////////
        /*
    */

    /**
     * Récupérer les aliments de la catégorie viande
     * @return
     */
    public ArrayList<Aliment> recupCatViande() {
        //creer une instance de realm
        realm= Realm.getDefaultInstance();
        //requete pour recuperer tous les produits ayant le type viande
        RealmResults<Aliment> query = realm.where(Aliment.class).contains("type","viandes").findAll();
        //fermer la transaction
        realm.commitTransaction();

        for (Aliment a : query
             ) {
           listeProduits.add(a);
        }
    return listeProduits;
    }


    /**
     *     pour recuperer l ensemble des produits ayant la denomination "poissons"
     * @return
     */
    public ArrayList<Aliment> recupCatPoissons() {
        //creer une instance de realm
        realm= Realm.getDefaultInstance();
        //requete pour recuperer tous les produits ayant le type poissons
        RealmResults<Aliment> query = realm.where(Aliment.class).contains("type","poissons").findAll();
        //fermer la transaction
        realm.commitTransaction();
        for (Aliment a : query
        ) {
            listeProduits.add(a);
        }
        return listeProduits;
    }


    /**
     *      pour recuperer l ensemble des produits ayant la denomination "laitages"
     * @return
     */
    public ArrayList<Aliment> recupCatLaitages() {
        //creer une instance de realm
        realm= Realm.getDefaultInstance();
        //requete pour recuperer tous les produits ayant le type laitages
        RealmResults<Aliment> query = realm.where(Aliment.class).contains("type","laitages").findAll();
        //fermer la transaction
        realm.commitTransaction();
        for (Aliment a : query
        ) {
            listeProduits.add(a);
        }
        return listeProduits;
    }


    /**
     *      pour recuperer l ensemble des produits ayant la denomination "fruits et legumes "
     * @return
     */
    public ArrayList<Aliment> recupCatFruitsEtLegumes() {
        //creer une instance de realm
        realm= Realm.getDefaultInstance();
        //requete pour recuperer tous les produits ayant le type fruits et legumes
        RealmResults<Aliment> query = realm.where(Aliment.class).contains("type","fruits_legumes").findAll();
        //fermer la transaction
        realm.commitTransaction();
        for (Aliment a : query
        ) {
            listeProduits.add(a);
        }
        return listeProduits;
    }


    /**
     *      pour recuperer l ensemble des produits ayant la denomination "divers"
     * @return
     */
    public ArrayList<Aliment> recupCatDivers() {
        //creer une instance de realm
        realm = Realm.getDefaultInstance();
        //requete pour recuperer tous les produits ayant la viande
        RealmResults<Aliment> query = realm.where(Aliment.class).contains("type", "divers").findAll();
        //fermer la transaction
        realm.commitTransaction();
        for (Aliment a : query
        ) {
            listeProduits.add(a);
        }
        return listeProduits;
    }


    /**
     * Récupérer tous les aliments
     * @param context
     * @return
     */
    public ArrayList<Aliment> recupAllAliments(Context context){
        Realm.init(context);
        realm = Realm.getDefaultInstance();
        listeProduits = new ArrayList<Aliment>();
        if(realm.isInTransaction() == false){
        realm.beginTransaction();}
        RealmResults<Aliment> query = realm.where(Aliment.class).findAll();
        realm.commitTransaction();

        for (Aliment a : query
        ) {
            listeProduits.add(a);
        }
        return listeProduits;
    }


    /**
     * Création d'aliment
     * @param aliment
     * @param context
     * @param listeNotifsAL
     * @param notification
     * @param listeDateNotif
     * @return
     */
    public boolean createAliment(Aliment aliment, Context context, ArrayList<Notification> listeNotifsAL, Notification notification, ArrayList<String> listeDateNotif) {
        // Initialize Realm (just once per application)
        Realm.init(context);

        // Get a Realm instance for this thread
        realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        sessionManager = new SessionManager(context);
        int userId = sessionManager.getSession();

        for(Notification notif : listeNotifsAL){
            Log.i("VERIFICATION ENTRE BOUCLE LISTENOTIFAL", "TAILLE ===========>"+listeNotifsAL.size());

            int index = listeNotifsAL.indexOf(notif);
            String dateNotif = listeDateNotif.get(index);
            Log.i("VERIFICATION ENTRE BOUCLE LISTENOTIFAL", "TAILLE ===========>"+listeNotifsAL.size());
            Log.i("VERIFICATION DATE NOTIF", "DATE ===========>"+dateNotif);
            Date date1 = null;
            try {
                date1=new SimpleDateFormat("dd/MM/yyyy").parse(dateNotif);
                Log.i("VERIFICATION CREATION DATE", ""+date1);

            } catch (ParseException e) {
                e.printStackTrace();
            }
            notif.setDateNotif(date1);
        }
        try {


            Number maxIdNotif = realm.where(Notification.class).max("idNotif");
            int newKeyNotif = (maxIdNotif == null) ? 1 : maxIdNotif.intValue() + listeNotifsAL.size()+1;
            Utilisateur utilisateur = realm.where(Utilisateur.class).equalTo("idUser", userId).findFirst();
            Number maxIdAliment = realm.where(Aliment.class).max("idAliment");
            int newKeyAliment = (maxIdAliment == null) ? 1 : maxIdAliment.intValue() + 1;
            notification.setIdNotif(newKeyNotif);
            notification.setAliment(aliment);
            listeNotifsAL.add(notification);

            if(utilisateur != null){
            aliment.setUtilisateur(utilisateur);}
            else {utilisateur = new Utilisateur(); utilisateur.setIdUser(0); aliment.setUtilisateur(utilisateur);}
            aliment.setIdAliment(newKeyAliment);
            Log.i("AVANT COPY TO REALM", "OK1");
            realm.copyToRealm(aliment);
            Log.i("APRES COPY TO REALM", "OK2");
            Log.i("INFOS ALIMENT", aliment.getDatePeremption()+" - "+aliment.getNom()+" - "+aliment.getIdAliment()+" - "+aliment.getUtilisateur().getIdUser());
            int compteur = 1;
                for (Notification n : listeNotifsAL) {
                   /* Notification notif = realm.where(Notification.class).equalTo("idNotif", n.getIdNotif()).findFirst();

                    if(notif == null){*/
                        n.setAliment(aliment);
                        Log.i("AVANT COPY TO REALM"+compteur, "OK1");
                        realm.copyToRealmOrUpdate(n);
                    Log.i("APRES COPY TO REALM"+compteur, "OK2");
                    Log.i("APRES COPY TO REALM IDNOTIF"+n.getIdNotif(), "VERIF ID NOTIF");
                        Log.i("NOMBRE NOTIFS LISTE", ""+listeNotifsAL.size());
                        Log.i("NOMBRE NOTIFS", ""+aliment.getListeNotifs().size());
                    compteur++;}
               /* }*/
            realm.commitTransaction();
            realm.close();
            return true;

        } catch (Exception e) {
           // Log.i("INFOS ALIMENT",aliment.getDatePeremption()+" - "+aliment.getNom()+" - "+aliment.getIdAliment()+" - "+aliment.getUtilisateur().getIdUser());
            realm.close();
            return false;

        }
    }
}
