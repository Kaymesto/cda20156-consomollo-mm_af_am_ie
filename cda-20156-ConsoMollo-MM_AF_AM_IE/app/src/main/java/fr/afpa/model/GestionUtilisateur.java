package fr.afpa.model;

import android.content.Context;

import fr.afpa.beans.Authentification;
import fr.afpa.beans.Utilisateur;
import fr.afpa.session.SessionManager;
import io.realm.Realm;
import io.realm.internal.Util;

public class GestionUtilisateur {

    private Realm realm;


    /**
     * Créer Compte
     * @param utilisateur
     * @param context
     * @return
     */
    public boolean createAccount(Utilisateur utilisateur, Context context) {
        // Initialize Realm (just once per application)
        Realm.init(context);

// Get a Realm instance for this thread
        realm = Realm.getDefaultInstance();
        if(realm.isInTransaction() == false){
        realm.beginTransaction();}
        try {

            Number maxIdUser = realm.where(Utilisateur.class).max("idUser");

            int newKeyUser = (maxIdUser == null) ? 1 : maxIdUser.intValue() + 1;

            Number maxIdAuth = realm.where(Authentification.class).max("idAuth");

            int newKeyAuth = (maxIdAuth == null) ? 1 : maxIdAuth.intValue() + 1;

            utilisateur.getAuthentification().setIdAuth(newKeyAuth);
            utilisateur.setIdUser(newKeyUser);

            realm.copyToRealm(utilisateur);
            realm.commitTransaction();
            realm.close();
            return true;
        }
        catch (Exception e){
            realm.close();
            return false;
        }
    }


    /**
     * Vérification si le login existe déja
     * @param utilisateur
     * @param context
     * @return
     */
    public boolean checkLoginExist(Utilisateur utilisateur, Context context) {
        Realm.init(context);
        realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        Authentification authentification = realm.where(Authentification.class).contains("login",utilisateur.getAuthentification().getLogin()).contains("password",utilisateur.getAuthentification().getPassword()).findFirst();
        if(authentification != null){
            realm.close();
            return true;}
        else {
            realm.close();
            return false;}


    }

    /**
     * Récupération de l'utilisateur pour la session
     * @param context
     * @return
     */
    public Utilisateur recupUserSession(Context context) {

        SessionManager session = new SessionManager(context);

        int idUser = session.getSession();
        Realm.init(context);

// Get a Realm instance for this thread
        realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        Utilisateur user = realm.where(Utilisateur.class).equalTo("idUser",idUser).findFirst();
        realm.close();
        return user;
    }
}
