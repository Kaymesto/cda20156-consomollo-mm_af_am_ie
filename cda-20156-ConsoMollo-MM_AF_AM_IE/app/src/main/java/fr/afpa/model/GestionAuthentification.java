package fr.afpa.model;

import android.content.Context;

import fr.afpa.beans.Authentification;
import fr.afpa.beans.Utilisateur;
import fr.afpa.cda_20156_consomollo_mm_af_am_ie.MainActivity;
import fr.afpa.session.SessionManager;
import io.realm.Realm;

public class GestionAuthentification {

    private Realm realm;

    /**
     * Vérification de connexion
     * @param login
     * @param password
     * @param context
     * @return
     */
    public boolean checkAccess (String login, String password,Context context) {

        // Récupérer utilisateur et envoyer le prénom dans la frame suivante

        if (!login.isEmpty() && !password.isEmpty()) {

        Utilisateur user =  sendUser(login,password,context);

        if (login.equals(user.getAuthentification().getLogin()) && password.equals(user.getAuthentification().getPassword()))

                return true;

        }

        return false;
    }

    /**
     * Recuperation de l'utilisateur si la connexion est OK
     * @param login
     * @param password
     * @param context
     * @return
     */
    public Utilisateur sendUser (String login, String password, Context context) {

        Realm.init(context);
        realm = Realm.getDefaultInstance();
        Utilisateur user = null;

        Authentification auth = realm.where(Authentification.class).contains("login",login)
                .contains("password",password).findFirst();

        if(auth!=null){
        user = realm.where(Utilisateur.class).equalTo
                ("authentification.idAuth", auth.getIdAuth()).findFirst();}
        return user;

    }


    /**
     * Enregistrer l'authentification
     * @param login
     * @param password
     */
    public void saveAuth (String login, String password) {
        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                Number maxId = realm.where(Authentification.class).max("idAuth");

                int newKey = (maxId == null) ? 1 : maxId.intValue() + 1;

                Authentification auth = realm.createObject(Authentification.class, newKey);
                auth.setLogin(login);
                auth.setPassword(password);
        realm.close();
            };


});
    }

}