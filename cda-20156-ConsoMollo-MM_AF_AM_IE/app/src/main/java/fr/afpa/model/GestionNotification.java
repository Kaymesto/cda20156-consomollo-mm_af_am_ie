package fr.afpa.model;

import android.content.Context;

import java.util.ArrayList;

import fr.afpa.beans.Aliment;
import fr.afpa.beans.Notification;
import io.realm.Realm;
import io.realm.RealmResults;

public class GestionNotification {
    private Realm realm;


    /**
     * Récupération de toutes les notifications
     * @param context
     * @param aliment
     * @return
     */
    public ArrayList<Notification> recupAllNotification(Context context, Aliment aliment){
        Realm.init(context);
        realm = Realm.getDefaultInstance();
        ArrayList<Notification> listeProduits = new ArrayList<Notification>();
        if(realm.isInTransaction() == false){
            realm.beginTransaction();}
        RealmResults<Notification> query = realm.where(Notification.class).equalTo("aliment.idAliment", aliment.getIdAliment()).findAll();
        realm.commitTransaction();

        for (Notification a : query
        ) {
            listeProduits.add(a);
        }
        return listeProduits;
    }


}
