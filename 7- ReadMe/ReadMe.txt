Bienvenue dans l'application ConsoMollo réalisée par Mohamed Meddah, Amal Farès, Adrien Maurer et Ibrahim EL Kadiri au sein de l'afpa (cda-20156).

Cette application est une application de gestion d'aliment afin de pouvoir lutter efficacement contre le gaspillage alimentaire.
Elle a pour vocation l'arrêt du gaspillage en permettant de créer des notifications afin de consommer ses produits avant la date de péremption.

Les pré-requis à l'utilisation de cette application sont :
- Un mobile Android (avec une version 7.0 minimum).
- Le fichier ConsoMollo.APK


Les utilisateurs pourront :
- créer des aliments
- créer des notifications associées à chaque aliment pour se rappeler de la date de péremption


Pour tout complément d'informations ou bugs à corriger:
 Merci de contacter l'un des administrateurs par email : 
- mmeddah59@gmail.com
- amal.fares@hotmail.fr
- adrienmaurer@hotmail.com
- ibrahim.elkadiri@yahoo.fr


Futures mises à jours :
- Ajouts de fonctionnalités diverses.
- Améliorations graphiques.
- Améliorations de la sécurité.